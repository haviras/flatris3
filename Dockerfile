# base image
FROM node
# create and go to folder
RUN mkdir /app


#For speedup build. Copy package.file (because docker get it from its cached layer) Наиболее частые изменения ставим в конец. Сначала максимально статичное
WORKDIR /app
COPY package.json /app
RUN yarn install
 COPY . /app

#run tests
#RUN yarn test



RUN yarn build
#RUN useradd -ms /bin/bash game \
#    && usermod -aG game game \ 




# start app in shell
CMD yarn start

# set port for app
EXPOSE 3000

#USER game
